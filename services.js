const fetch = require('node-fetch')

const getNameEmailCompany = async (URL)=>{
    let aUsers= await load(URL)
    let list = await aUsers.map((x)=>{
        return {"name":x.name,
                "email":x.email,
                "company":x.company}
    })
    list = list.sort((a,b)=>{
        let nA = a.company.name.toLowerCase()
        let nB = b.company.name.toLowerCase()
        if (nA<nB){
            return -1
        }else if(nA>nB){
            return 1
        }else{
            return 0
        }
    })
    return list           
}

const getWebSites = async (URL)=>{
    let aUsers=await load(URL)
    return await aUsers.map((x)=>x.website)
}

const getSuite = async (URL)=>{
    let aUsers=await load(URL)
   return await aUsers.filter((x)=>x.address.suite.match(/Suite/g))   
}

const route = (path)=>[`/${path}/:mode`,`/${path}*`]

const manageResponse = (name,list,mode,response)=>{
    if(mode==='demo'){
        response.render(name, {arr:list})
    }else if(mode===undefined){
        response.json(list)
    }else{
        response.send('INVALID PARAM')
    }
}

const load = async (URL)=>{
    let aUsers=[]
    await fetch(URL)
            .then(response => response.json()) 
            .then(result => {
                aUsers=result
            })
            .catch(err => {
                console.error('FALHA AO TENTAR OBTER CATÁLOGO! ', err)
            })
    return aUsers
}
module.exports = {getNameEmailCompany,getWebSites,getSuite,manageResponse,route}