const path = require('path');
const express = require('express')
const serv = require('./services')

const PORT = process.env.PORT || 8080
const URL = 'https://jsonplaceholder.typicode.com/users'

const app = express()

app.use(express.static(path.join(__dirname, 'public')));
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'pug')

app.get('/', (req, res) => {
    res.render('index')
})

app.get('/demo', (req, res) => {
    res.render('demo')
})

app.get(serv.route('websites'), async (req, res) => {
    let list = await serv.getWebSites(URL)
    serv.manageResponse('websites', list, req.params.mode, res)
})

app.get(serv.route('simpledata'), async (req, res) => {
    let list = await serv.getNameEmailCompany(URL)
    serv.manageResponse('simpledata', list, req.params.mode, res)
})

app.get(serv.route('suite'), async (req, res) => {
    let list = await serv.getSuite(URL)
    serv.manageResponse('suite', list, req.params.mode, res)
})

app.listen(PORT, async () => {
    console.log(`
             
                  .           '||                                     .  
  ....   ....   .||.   ....    ||    ...     ... .  ....   .. ...   .||. 
.|   '' '' .||   ||   '' .||   ||  .|  '|.  || ||  '' .||   ||  ||   ||  
||      .|' ||   ||   .|' ||   ||  ||   ||   |''   .|' ||   ||  ||   ||  
 '|...' '|..'|'  '|.' '|..'|' .||.  '|..|'  '||||. '|..'|' .||. ||.  '|.'
                                           .|....'                       
                                                       
    Use a porta ${PORT}!
                            `)
});